---
title: 'Trie Data Structure'
---

## Description
> Trie is an efficient information reTrieval data structure. Using Trie, search complexities can be brought to optimal limit (key length). If we store keys in binary search tree, a well balanced BST will need time proportional to M * log N, where M is maximum string length and N is number of keys in tree. Using Trie, we can search the key in O(M) time. However the penalty is on Trie storage requirements.

Source: https://www.geeksforgeeks.org/trie-insert-and-search/

## Structure
```c
struct TrieNode
{
     struct TrieNode *children[ALPHABET_SIZE];

     // isEndOfWord is true if the node
     // represents end of a word
     bool isEndOfWord;
};
```

## Diagram
[mermaid]
graph TD
    A((A)) --> a1((A))
    A --> b1((B))
    A --> c1((C))
    A --> d1((D))
    c1 --> a2((A))
    c1 --> b2((B))
    c1 --> c2((C))
    c1 --> d2((D))
style A stroke:#333,stroke-width:4px
style b2 stroke:#333,stroke-width:4px
[/mermaid]