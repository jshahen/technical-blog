---
title: 'Logic Locking'
---

The term Logic Locking deals with circuitry and intellectual property.

Logic locking is a technique of taking a circuit and through adding and changing that circuit, produce a new circuit that contains a set of KEY wires, and when the correct key bit string is provided, then the locked circuit works exactly as the original circuit. If another key is provided then the output should not be equal to the unlocked circuit for all of the inputs.