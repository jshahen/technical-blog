---
title: 'Compute Canada 2019+'
---

Here are the steps to getting started with Compute Canada as a grad student:

1. Have your advisor/supervisor setup a PI account with Compute Canada
2. Get your advisor's CCRI and use that to create an account with Compute Canada
    * If you already have an account, then use the CCRI to create a new role and set that role as your primary (there is a link once logged in for this)
3. Login to compute canada website
4. Request an consortium account
5. Select Niagara and follow steps (as of publishing, no addition resources are required for a waterloo grad)
6. WAIT for an email from compute canada saying your account with Niagara was created
7. SSH into Niagara using your ID (can be found on compute canada) and your normal password (SSH URL: niagara.scinet.utoronto.ca, PORT: 22)
8. Login to their Jupyter Hub (URL: https://jupyter.scinet.utoronto.ca/)