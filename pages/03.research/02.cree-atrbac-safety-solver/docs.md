---
title: 'Cree: ATRBAC-Safety Solver'
---

Cree is an [ATRBAC-Safety](/math/atrbac-safety) solver where the solver reduces the ATRBAC-Instance to [Model Checking](/math/model-checking) and solves with an off-the-shelf model checker (currently utilizes [NuSMV](http://nusmv.fbk.eu/)).

Along with the reduction to Model Checking, Cree implements many ATRBAC-Safety specific optimizations and techniques to increase solving speed for the benchmarks available.

Cree is currently under review, but expected to be published very soon.

# A Diagram of Cree
[mermaid]
graph LR;
    A[Start]-->B((q));
    B-->C[Static Pruning];
    C-->D((q));
    D-->E[Abstraction Refinement];
    E --> f((q));
    f --> g[Bound Estimation];
    g --> h[Reduce to Model Checking];
    h --> i{NuSMV};
    i --> r(Result);
    B-->r;
    D --> r;
    f --> r;
    i --> E;
[/mermaid]

# Cree Components

## Quick Decisions (q)


## Static Pruning


## Abstraction Refinement


## Bound Estimation


## Reduction to Model Checking

