---
title: Research
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 3

# Research

This chapter focuses on subjects learned directly from research.
