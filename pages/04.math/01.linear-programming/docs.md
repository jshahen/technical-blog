---
title: 'Linear Programming'
---

The content in this section are taken from CO602 and other sources as cited.

## Convex
* a set $S \subseteq R^n$ is **convex** if, for each $x,y \in S$ and $\lambda \in [0,1]$, $\lambda x + (1-\lambda)y \in S$
* a function $f : S \rightarrow R$ is **convex** if, for each $x,y \in S$ and $\lambda \in [0,1]$, $f(\lambda x + (1-\lambda)y) \le \lambda f(x) + (1-\lambda)f(y)$

### Convex Hull


## Creating the Dual of a Linear Program


### Implied Inequalities


## Weak Duality Theorem


## Strong Duality Theorem


## Complementary Slackness Conditions


## Fourier-Motzkin Elimination


## Polyhedra
* A **polyhedron** is a set of the form $\{x \in R^n : Ax \ge b\}$, where $A \in R^{m \times n}$ and $b \in R^m$.
* A **polytope** is a bounded polyhedron
* If $P \subseteq R^n$ is a polyhedron and $P'$ is the projection of $P$ onto $x_1, \ldots, x_l$ (where $l < n$), then $P'$ is a polyhedron
  **Proof:** Use Fourier-Motzkin Elimination to prove this.

## Fundamental Theorem of Linear Algebra (made up name)
Let $F$ be a field. For $A \in F^{m \times n}$ and $b \in F^m$ exactly one of the following systems has a solution:

1. $Ax = b, x \in R^n $
1. $y^TA = 0, y^Tb = 1, y \in R^m$

>>>>> If $Ax = b$ is infeasible, then we can obtain the equation $0=1$ by taking a linear combination of the rows (which is extressed in 2)

## Farkas Lemma
>>>>> This lemma is very important and can be used in conjuction with the Weak Duality and Strong Duality Theorem to prove many other theorems
>>>>> This lemma is very similar to the Fundamental Theorem of Linear Algebra

Let $A \in R^{m \times n}$ and $b \in R^m$ exactly one of the following systems has a solution:

1. $Ax \ge b, x \in R^n $
1. $y^TA = 0, y^Tb = 1, y \ge 0, y \in R^m$

>>>>> If $Ax \ge b$ has no solution, then we can obtain the inequality $0\ge1$ by taking a non-negative combination of the constraints
