---
title: 'Linear Algebra'
---

## Properties of Matrix Operations

### Properties of Addition

Let $A$, $B$ and $C$ be $m \times n$ matrices

1. $A + B = B + A$ **(commutative)**
1. $A + (B + C) = (A + B) + C$ **(associative)**
1. There is a unique $m \times n$ matrix $O$ with
   $A + O = A$ **(additive identity)**
1. For any $m \times n$ matrix $A$ there is an $m \times n$ matrix $B$ (called $\neg A$) with
   $A + B = O$ **(additive inverse)**
   
### Properties of Matrix Multiplication
Let $A$, $B$ and $C$ be matrices of dimensions such that the following are defined.  Then

1. $A(BC) = (AB)C$ **(associative)**
1. $A(B + C) = AB + AC$ **(distributive)**
1. $(A + B)C = AC + BC$ **(distributive)**
1. There are unique matrices $I_m$ and $I_n$ with
   $I_m A  =  A I_n  =  A$ **(multiplicative identity)**
   
### Properties of Scalar Multiplication
Let $r$ and $s$ be real numbers and $A$ and $B$ be matrices. Then

1. $r(sA) = (rs)A$
1. $(r + s)A = rA + sA$
1. $r(A + B) = rA + rB$
1. $A(rB) = r(AB) = (rA)B$

### Properties of the Transpose of a Matrix
Let $r$ be a real number and $A$ and $B$ be matrices.  Then

1. $(A^T)^T  =  A$
1. $(A + B)^T  =  A^T + B^T$
1. $(AB)^T  =  B^TA^T$ (This is used in proofs for [Linear Programming](/math/linear-programming))
1. $(rA)^T  =  rA^T$