---
title: 'Safety Problem'
---

The Safety Problem is a decision problem in Access Control which deals with whether a User is able to gain access to a set of permissions.