---
title: ATRBAC-Safety
---

Administrative Temporal Role Based Access Control (ATRBAC) is model of [Access Control](https://en.wikipedia.org/wiki/Access_control). ATRBAC-Safety is the [Safety Problem](/math/safety-problem) applied to ATRBAC.

## Locked Room Maze Analogy
![](maze-large.png)
