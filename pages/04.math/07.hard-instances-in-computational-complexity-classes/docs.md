---
title: 'Hard Instances in Computational Complexity Classes'
taxonomy:
    category:
        - computation-complexity
        - math
    author:
        - 'Jonathan Shahen'
menu: 'Hard Instances in Complexity'
---

Through out my MASc and PhD I have been researching computer engineering problems which reside non-polynomial complexity classes.
This classically means that the problems are **hard**.

During my research I would need to test my theories and show empirical evidence supporting my claims. This entails running code against problem instances.
A unique decision arises in research where the choice of problem instances can determine how valid your results are towards your claim.

Is it better to have problem instances:
 - That appear most frequently in the wild?
 - That are randomly generated?
 - That are purposely crafted to tax the solver for very small instances?


# Classes of Input
A class of input, is a set, potentially infinite in size, of problem instances that share a common property.

The common shared property could be how they were generated. Another example is what specific properties exists within the instances.

Some classes of input are very useful as we can show that certain properties exists for all elements in that class.
An example property would be to show that for particular class of inputs the complexity class is different from the base class (NP-Hard problem has infinite class of inputs that can be solved in polynmoial time).

# Hard Instances are Solver Dependent


# Working Definition of 'Hard' Instances
In this section I will try and define what a hard instance is.
Note that I say 'try', because knowing the exact definition of why a complexity class is hard can be one way to solve the open problem of $P \subseteq NP$

# Hard Instances in ATRBAC-Safety
The subject of hard instances in [ATRBAC-Safety](/math/atrbac-safety) is the subject of my paper: [Hard Instances in ATRBAC-Safety](/research/hard-instances-in-atrbac-safety).

# Hard Instances in Logic Locking
