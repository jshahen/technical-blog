---
title: Co-NP
---

>  A decision problem X is a member of co-NP if and only if its complement X is in the complexity class NP. In simple terms, co-NP is the class of problems for which there is a polynomial-time algorithm that can verify "no" instances (sometimes called counterexamples) given the appropriate certificate. Equivalently, co-NP is the set of decision problems where the "no" instances can be accepted in polynomial time by a non-deterministic Turing machine. [[1]]
  

[1]: https://en.wikipedia.org/wiki/Co-NP