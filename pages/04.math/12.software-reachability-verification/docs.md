---
title: 'Software Reachability (Verification)'
---

## Halting Problem
Important terms:

- **Decision problems** - A decision problem is a problem that can be posed as a yes-no question of the input values.
- **Turing machine** – A Turing machine is a mathematical model of computation. Which manipulates symbols on a strip of tape according to a table of rules. Given any computer algorithm, we can construct a Turing machine capable of simulating that algorithm's logic.
- **Halting** - A program/algotithm/machine, $P$, Halts for input $X$, if when given the input $X$ then $P$ will eventually finishing executing. A program with an infinite loop and no exit condition does not halt.


## Reference Links
- [1] https://plato.stanford.edu/entries/computability/#HalPro
- [2] https://www.geeksforgeeks.org/halting-problem-in-theory-of-computation/