---
title: 'Formal Verification'
---

## Reference Links
 - [1] https://en.wikipedia.org/wiki/Formal_verification
 - [2] https://en.wikipedia.org/wiki/Static_program_analysis
 - [3] https://en.wikipedia.org/wiki/Temporal_logic_in_finite-state_verification
 - [4] https://en.wikipedia.org/wiki/Software_verification_and_validation