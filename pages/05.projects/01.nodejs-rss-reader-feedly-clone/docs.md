---
title: 'Node.js RSS Reader (Feedly Clone)'
---

This project was inspired by the fact that I had reached the limit of free feeds on the online RSS reader "Feedly".
I switched to Feedly when Google's RSS reader closed down.

At the time I was just getting into NodeJS, which is what inspired me to use that technology for the backend.
For the frontend, I copied the struture of Feedly so that I could quickly get a useable product. 
For the backend, it is a split of nodejs for the web-backend and python for the webscraper.

This project has provided me with many techniques for how websites limit web crawlers and the many techniques to circumvent these web crawlers.
I have also gained valuable experience with asyncronious requests and web sanitizing.

# Diagram
[mermaid]
graph LR
python --> pg;
    subgraph Database
    pg[Postgres]
    end
    subgraph Web-Scraper
    python[Multi-Threaded<br>Custom Python 3]-->requests[Requests]
requests --> extra[Extra Information]
extra --> sanitize[Sanitize]
    end
    subgraph web-server
    node[Nodejs]-->express[Express JS]
    end
node --> pg;
[/mermaid]