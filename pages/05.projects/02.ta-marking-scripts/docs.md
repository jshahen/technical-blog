---
title: 'TA Marking Scripts'
---

I have been a TA numerous terms while studying for my MASc and my PhD.

Unlike in other STEM fields, in Computer Engineering, TA jobs are not mandatory nor are they attached to every students scholarship.
In my case, my advisor was able to afford my scholarship without need of resorting to a mandatory TA position.

During my last TA position, I wrote a series of python scripts, which when run together allows a TA to quickly get a system built up for marking submitted assignments.
Below is the included `README.md` file from the project.

---

# Critical Reminder

The scripts contained in this repo do not contain any sensitive information, but all student submissions and grades are considered **Highly Sensitive**, and thus if you create a new repo to track the grades and submissions, make sure that it is hosted within the UWaterloo domain and that it is marked **PRIVATE**.

# Version v1.4

Please look at the `git.uwaterloo.ca` page to see if there are any updates to the scripts you are using.

# 0. Requirements

1. Python 3
1. `pip install openpyxl patool tqdm`
   - `patool` requires an external tool in order to extract other archieve types besides `{zip,tar,gz}`.
   - 7zip is suggested to provide other archive extraction support. It must be available on your path.
   - **If you know you will only see .zip files, then 7zip is not required**
1. Latex Installation (this is what the feedback files are created using)\]
   - TeX Live is suggested above MikTex
   - `pdflatex` must be on your PATH

# 1. Steps Before Marking

1.  Download the entire dropbox via Learn (it will give you a zip file)
    1. Go to Menu Item `Submit` and then click on the sub-menu item `Dropbox`
    1. Click on the dropbox you want to grade (ex: Dropbox for Assignment 1)
    1. Scroll to the bottom and edit the dropdown box to be `200 per page`
    1. Scroll to the top and select the checkbox that will check all submissions (should be to the left of `First Name, Last Name`)
    1. Click download and save the zip to this folder
1.  Run `python 1_extract_zip.py zip_filename.zip`
    - replace `zip_filename.zip` with your dropbox zip
    - run using python 3
    - example `python 1_extract_zip.py "Dropbox for Assignment 1 Download Sep 12, 2019 138 PM.zip"`
    - After this occurs you will have a new folder: `submissions`
1.  Run `python 2_setup_xlsx.py <XLSX_filename> (<marks_for_each_question>)`
    - Follow the on screen instructions
    - The XLSX file **must NOT exist**
    - There is a quick way to run this command (utilize the optional parameter: `marks_for_each_question`):
      - Example command: `python 2_setup_xlsx.py grades.xlsx 5,5,4,2,1`
      - Creates `grades.xlsx`
      - Question 1: 5 marks
      - Question 2: 5 marks
      - Question 3: 4 marks
      - Question 4: 2 marks
      - Question 5: 1 marks

# Optional Scripts to Help With Marking

1. `2a_optional_run_submissions.py`
   - This requires manual updating!

# Marking Cirteria

This section should be updated with specifics from the professor of the course.

# 3. Marking Dropbox

1. Open the XLSX file that was created in the above step
1. Open the folder `submissions` and navigate to the first folder or file
1. Wrtie your grades/feedback in the corresponding row and column in the XLSX
1. On the second sheet in the XLSX, write any common feedback you have and then use the key in the `Grades` sheet where ever you want to use it.

# 4. Create Feedback PDF Files

**NOTE:** _You must be completely finished marking before starting this step!_

1. Run `python 3_create_feedback.py <XLSX_filename>`
   - Same filename as created before
   - Follow the on screen instructions
1. Feedback pdfs will be written to `feedback_pdfs`
1. Zip all files within the folder (DO NOT ZIP THE FOLDER!)
1. Now you can upload the feedback files
   1. Go to the dropbox (`Submit` -> `Dropbox` -> Click on your dropbox)
   1. Click `Add Feedback Files`
   1. Upload the zip of all pdf files
1. **Upload the grades first, then continue back to this step**
1. Select the checkbox that will select all submissions (should be to the left of `First Name, Last Name`)
1. Click `Publish Feedback`

# 5. Uploading the grades

1. Download the dropbox grades from Learn
   1. Go to the `Grades` Menu on Learn
   1. Click on the sub-menu `Enter Grades`
   1. Click on the button `Export`
   1. Edit the feilds:
      - All Users
      - Both
      - Points grade
      - Last Name
      - First Name
      - Dropbox for Assignment 1
      - Only select the dropbox you are marking!
   1. Click `Export to CSV`
      - You should get a file with the similar name to: `ECE 606 - Fall 2019_GradesExport_2019-09-12-20-02.csv`
1. Run `python 4_update_gradebook.py <XLSX_filename> <Dropbox_GradesExport_From_Learn>`
1. Upload the new grades file and verify that it worked
   1. Go to the `Grades` Menu on Learn
   1. Click on the sub-menu `Enter Grades`
   1. Click on the button `Import`
   1. Choose your newly created csv file (prefixed with `GRADED_`)
   1. Click `Continue`
   1. Submit grades if no issues popup
