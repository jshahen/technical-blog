---
title: Projects
---

This section contains project I have either solely worked on, or worked on as part of a team.

Each project was created due to some need I had, or was associated with the needs of a friend/family member.