---
title: 'Text Processing'
taxonomy:
    author:
        - 'Jonathan Shahen'
external_links:
    target: _blank
---

## Tokenization


## Normalization


### Stemming


### Lemmatization


## Noise Removal


## References
- [Reference 1][1]
- [Reference 2][2]
- [Reference 3][3]
- [Reference 4][4]
- [Reference 5][5]

[1]: https://www.kdnuggets.com/2017/12/general-approach-preprocessing-text-data.html
[2]: https://raghakot.github.io/keras-text/keras_text.processing/
[3]: https://towardsdatascience.com/how-to-preprocess-character-level-text-with-keras-349065121089
[4]: https://mlwhiz.com/blog/2019/01/17/deeplearning_nlp_preprocess/
[5]: https://keras.io/preprocessing/text/