---
title: 'Ethereum Terminology'
media_order: 'ethereum_units.png,ethereum_costs.png,ethereum_example_transaction_costs.png,ethereum_example_gar_to_ether.png'
---

## 1. Enthereum vs BitCoin
Reference [[3]]

**Bitcoin** wants to be a global decentralized financial system while empowering people to have full control over their finances. While Bitcoin does lack the scalability to be a traditional currency system, it has the capability of becoming a digital store-of-value.

**Ethereum**, is not a payment-only system. By leveraging the blockchain technology, developers can create real-world applications on top of it. The way they can do that is by creating smart contracts and executing them on top of Ethereum.
A Smart Contract is computer code running on top of a blockchain containing a set of rules under which the participants of the contract agree to interact with each other. There are certain features of smart contract interactions:

1. The participants of the smart contract can directly interact with each other without the need for a middleman or a third-party.
1. Each step of a smart contract can only be implemented after the execution of the immediate former step.
1. The smart contract acts as a blueprint for a decentralized application (DApp).
1. All the contents and data inside a DApp is not owned by one single entity.

## 2. Smart Contracts

Very small programs (couple hundred lines of code) that once added to the blockchain cannot be changed.

Smart contracts state they are turing complete, and have the ability to perform loops, arithermetic operations, and store state.

When a smart contract is run, it is run on all nodes within the etherium blockchain (6880 nodes as of Oct. 23 2019 -- fluctuates frequently)


### Accounts

**External Account:** owned by a person

**Contract Accounts:** owned by a contract, no private key is set.

You cannot determine if an account is external or contract owned just from the address.

## 3. Cost
Transactions can cost money, are the purpose of this is to prevent spam and to solve the haulting problem.

If a function only reads from the blockchain, and uses the `view` function declarator, then it is a free transaction.

### Units
![](ethereum_units.png)

### Gas
* On the ethereum blockchain, 'gas' refers to the cost necessary to perform a transaction on the network.
* Miners set the price of gas and can decline to process a transaction if it does not meet their price threshold.
* Gas prices are denoted in gwei, with are worth 0.000000001 ether.
* Having a separate unit allows maintaining a distinction between the actual valuation of the cryptocurrency, and the computational cost.

### Transaction and Execution
* **Transaction costs** are based on the cost of sending data to the blockchain. There are 4 items which make up the full transaction cost:
    * the base cost of a transaction (21000 gas)
    * the cost of a contract deployment (32000 gas)
    * the cost for every zero byte of data or code for a transaction.
    * the cost of every non-zero byte of data or code for a transaction.
* **Execution costs** are based on the cost of computational operations which are executed as a result of the transaction.
* **Total Cost:** Transaction costs + execution costs

![](ethereum_costs.png)

### Example Ethereum Tranaction
![](ethereum_example_transaction_costs.png)

The total cost in Ether [[8]] (about 0.5 cents CAD [[9]]):
![](ethereum_example_gar_to_ether.png)

## 4. Ethereum Backend

### Ethereum Cyrpotcurrency


### Ethereum Virtual Machine (E.V.M.)


### Proof-of-Stake vs Proof-of-Work
References: [[4]], [[5]]

Bitcoin is Proof-of-Work, Ethereum is Proof-of-Work but they are looking to convert to Proof-of-Stake in 2021 [[10]].

### Uncle Block
* References: [[12]]

Uncle blocks are the Ethereum equivalent of Bitcoin’s orphan block, with a few differences. Like orphan blocks, which are associated with Bitcoin, uncle blocks are commonly associated with Ethereum-based blockchains. Similar to Bitcoin’s orphan blocks, uncle blocks are valid and are mined in a genuine manner, but get rejected from the main blockchain due to the working mechanism of the blockchain. 

## 5. Blockchain/Ethereum Math
### Byzantine fault
Reference: [[6]], [[7]]

A Byzantine fault is a condition of a computer system, particularly distributed computing systems, where components may fail and there is imperfect information on whether a component has failed.

In a Byzantine fault, a component such as a server can inconsistently appear both failed and functioning to failure-detection systems, presenting different symptoms to different observers. It is difficult for the other components to declare it failed and shut it out of the network, because they need to first reach a consensus regarding which component has failed in the first place.

One example of BFT in use is bitcoin, a peer-to-peer digital cash system. The bitcoin network works in parallel to generate a blockchain with proof-of-work allowing the system to overcome Byzantine failures and reach a coherent global view of the system's state.

**Byzantine Fault Tolerant Systems** is a distributed system (ex: blockchain) where the system will act correctly even if a minority of the nodes do not receive messages or act in a malicious manner. There are multiple consensis algorithms that can be used in a blockchain to force the system to be BFT: Proof-of-Work, Proof-of-Stake, and Delegated Proof-of-Stake.

## 6. Decentralized Applications (DApps)


## 7. Decentralized Autonomous Organization (DAO)
References: [[11]]

DAO is a unique concept that took a hint from Bitcoin to operate businesses without a hierarchical management system. In layman’s term, its a smart contract running an intelligent algorithm that can carry out decisions based on the information and without intervention or presence of traditional management.
Since no single individual or group of individuals have control over the operations, the process remains unaffected in the wake of any structural failure, thanks to its decentralized nature.

The biggest problem with DAO was its rigidity. Meaning the unstoppable code once deployed on ethereum blockchain network could pose a security threat because once it is let loose, no one could change the code. The drawback is subjective: In some cases, it would prevent tampering. But, other times if someone with malicious intent finds a flaw or vulnerability, he can exploit it for personal gains.

There has been a case of exploitation with The DAO where hackers found a bug, and drained all the funds slowly and those who became aware of this attack couldn’t do anything but watch.

## 8. Example Ethereum Applications
1. https://www.stateofthedapps.com/platforms/ethereum -- Tons of DApps, lots of information, really good for Categories
1. https://etherscan.io/dapp -- List of DApps that are popular and actually used
1. https://www.stateofthedapps.com/dapps/platform/ethereum?tags=token&page=1 -- Ethereum DApps that mention Token
1. https://www.stateofthedapps.com/dapps/platform/ethereum?page=1&status=broken -- List of Ethereum DApps that are labelled as Broken


[1]: https://etherscan.io/nodetracker
[2]: https://medium.com/linum-labs/ethereum-tokens-explained-ffe9df918008
[3]: https://blockgeeks.com/guides/bitcoin-vs-ethereum-ultimate-comparison-guide/
[4]: https://www.youtube.com/watch?v=M3EFi_POhps
[5]: https://www.bitdegree.org/tutorials/proof-of-work-vs-proof-of-stake/
[6]: https://en.wikipedia.org/wiki/Byzantine_fault
[7]: https://www.youtube.com/watch?v=VWG9xcwjxUg
[8]: https://gwei.io/
[9]: https://www.google.com/search?q=0.000024664+ether+to+cad
[10]: https://github.com/ethereum/wiki/wiki/Proof-of-Stake-FAQ
[11]: https://hackernoon.com/decentralized-autonomous-organization-the-future-of-management-f61l830nv
[12]: https://www.investopedia.com/terms/u/uncle-block-cryptocurrency.asp