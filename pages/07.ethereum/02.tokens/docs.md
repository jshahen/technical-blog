---
title: Tokens
---

* The popular cryptocurrency and blockchain system known as Ethereum is based on the use of tokens, which can be bought, sold, or traded.
* One of the most significant tokens is called ERC-20, which has emerged as the technical standard used for all smart contracts on the Ethereum blockchain for token implementation.
* Because the ERC-20 standard remains relatively nascent, there will likely be bugs that need to be worked out, as Ethereum continues to develop.

## Token Terms
Reference [[2]]
* **Native token:** The token of the blockchain that your token exists within. E.g: Ether is the native token of the Ethereum blockchain. DAI is a token created within Ethereum but is not the native token. It can often be referred to as a coin.
* **ERC:** Ethereum Request for Comment. This is used for any suggestion to improve Ethereum and not only for tokens. Community members comment on these requests, which then either gets accepted or rejected by the general community.
* **EIP:** Ethereum Improvement Proposals. Once an ERC has received enough positive feedback it gets formalised into an EIP. It then gets audited and assessed by the core Ethereum developers.
* **The difference between ERCs and EIPs:** There are hundreds of standards that address issues beyond the scope of tokens. A full list of formally proposed standards can be found here for EIPs. ERCs are more informal initial proposals vetted by the community. You can take a look at the Ethereum GitHub to learn more about these standards as the Ethereum community is constantly bursting with new and fresh ideas.
* **Fungible:** One is equal to the other. For example, one dollar is always equal to another dollar.
* **Non-fungible:** One is not equal to another. For example, artworks. If we traded one for another we are not getting equal value. Each non-fungible item is unique.
* **Spender:** An address that gets approved by the token owner. Spenders are approved to spend a specified amount of tokens (with fungible tokens) or approved to spend a specific token (in the case of non-fungible tokens) on behalf of the user. I.E: the user may approve the spender once and the spender may spend on the user’s behalf until their allowance is finished.
* **msg.sender:** The address that calls the contract’s function.
* **msg.value:** The amount of Ether (in wei) sent with the function call.

## ERC-20 Tokens
* ERC-20:  https://eips.ethereum.org/EIPS/eip-20

### Lost Tokens Due to ERC-20 Bug

1. QTUM, $1,204,273 lost. (https://etherscan.io/address/0x9a642d6b3368ddc662CA244bAdf32cDA716005BC)[watch on Etherscan]
1. EOS, $1,015,131 lost. (https://etherscan.io/address/0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0)[watch on Etherscan]
1. GNT, $249,627 lost. (https://etherscan.io/address/0xa74476443119A942dE498590Fe1f2454d7D4aC0d)[watch on Etherscan]
1. STORJ, $217,477 lost. (https://etherscan.io/address/0xe41d2489571d322189246dafa5ebde1f4699f498)[watch on Etherscan]
1. Tronix , $201,232 lost. (https://etherscan.io/address/0xf230b790e05390fc8295f4d3f60332c93bed42e2)[watch on Etherscan]
1. DGD, $151,826 lost. (https://etherscan.io/address/0xe0b7927c4af23765cb51314a0e0521a9645f0e2a)[watch on Etherscan]
1. OMG, $149,941 lost. (https://etherscan.io/address/0xd26114cd6ee289accf82350c8d8487fedb8a0c07)[watch on Etherscan]

## ERC-721 Tokens
* ERC-721: https://eips.ethereum.org/EIPS/eip-721


## ERC-777 Tokens
* ERC-777: https://eips.ethereum.org/EIPS/eip-777

## Link Dump
**ERC Links:**
* https://eips.ethereum.org/erc
* https://eips.ethereum.org/EIPS/eip-20
* https://github.com/ethereum/EIPs/issues/223
* https://eips.ethereum.org/EIPS/eip-721
* https://eips.ethereum.org/EIPS/eip-777
* https://eips.ethereum.org/EIPS/eip-1155

**Raw Solidity:**
* https://github.com/OpenZeppelin/openzeppelin-contracts/blob/9b3710465583284b8c4c5d2245749246bb2e0094/contracts/token/ERC20/ERC20.sol
* https://github.com/OpenZeppelin/openzeppelin-contracts/tree/9b3710465583284b8c4c5d2245749246bb2e0094/contracts/token
* https://github.com/ConsenSys/Tokens/blob/fdf687c69d998266a95f15216b1955a4965a0a6d/contracts/eip20/EIP20.sol

**Explanation:**
* https://medium.com/@brenn.a.hill/noobs-guide-to-understanding-erc-20-vs-erc-721-tokens-d7f5657a4ee7
* https://101blockchains.com/erc20-vs-erc223-vs-erc777/
* (https://www.youtube.com/watch?v=cqZhNzZoMh8&vl=en)[Youtube - ERC20 tokens - Simply Explained]

**DAPPs:**
* https://www.stateofthedapps.com/dapps/cryptokitties
* https://www.stateofthedapps.com/dapps?tags=token