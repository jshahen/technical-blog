---
title: Escrow
media_order: escrow.png
---

## Escrow Basics
Reference: [[1]]

Escrow is a financial arrangement in which two parties enlist a “third party” (who is neither the buyer nor the seller) to temporarily hold money, paperwork, or other assets for a transaction on their behalf before the transaction has been finalized.

That third party, known as an escrow provider, helps make the transaction safer by protecting the assets of the buyer and seller until both parties have met their obligations for the agreement. Ideally, the escrow provider is a neutral third party who isn't concerned with whether the buyer or seller comes out ahead.

## How Escrow Works
References: [[1]], [[2]]

![](escrow.png)

When you commit to buying or selling something, you agree to fulfill certain terms. For example, the buyer must pay the agreed-upon amount by a specific time, and the seller must provide the asset being sold. Of course, most transactions are more complicated than that. For example:

Buyers might want the right to inspect the property or goods they are buying before paying.
Sellers might want some assurance that they'll get paid (or have the opportunity to move on if the deal isn't happening quickly enough).
The item being sold might be a service instead of a product.
In complicated arrangements like these, one party may feel unsure that the other will meet their end of the bargain, creating the need for a third party to act as a "referee." The escrow provider acts as this middleman and ensures that the buyer and seller do what they agreed to do.

The escrow provider's responsibilities in a transaction include receiving assets from one party, disbursing funds according to the terms of the escrow agreement, and closing escrow. Their role in the transaction safeguards the assets of buyers and sellers before they get transferred from one party to the other.

Of course, given the ample assets at stake in big transactions, you should use a trusted escrow provider—a big-name escrow company or a service provider recommended by your real estate agent. Do your due diligence and search for the company online with the word "complaint" to dredge up any negative reports. Likewise, check to see if the provider must be licensed in the state in which it operates—and then confirm that it is licensed.

Escrow can be used in any number of financial and legal scenarios where something of value exchanges hands from one party to another. But it frequently applies to real estate and online transactions.

## OpenZepplin Escrow Smart Contract
Reference: [[3]]



[1]: https://www.thebalance.com/what-is-escrow-315826
[2]: https://www.hackers-list.com/wp-content/uploads/2018/08/escrow.png
[3]: https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/payment/escrow/Escrow.sol