---
title: 'Initial Coin Offering (ICO)'
media_order: ico_structure.png
---

**KEY TAKEAWAYS**
* Initial Coin Offerings (ICOs) are a popular fundraising method used primarily by startups wishing to offer products and services, usually related to the cryptocurrency and blockchain space.
* ICOs are similar to stocks, but they sometimes have utility for a software service or product offered. 
* Some ICOs have yielded massive returns for investors. Numerous others have turned out to be fraud or have failed or performed poorly. 
* To participate in an ICO, you will usually need to purchase a digital currency first and have a basic understanding of how to use cryptocurrency wallets and exchanges. 
* ICOs are, for the most part, completely unregulated, so investors must exercise a high degree of caution and diligence when researching and investing in ICOs.
* Reference: [[1]]

![](ico_structure.png)

## Current ICOs
According to [[2]], there are 227,244 Token Contracts (as of November 26th, 2019). Not all of these are ICOs, but this does give an upper bound on the number of ICOs there are at the time of writing.

[1]: https://www.investopedia.com/terms/i/initial-coin-offering-ico.asp
[2]: https://etherscan.io/tokens