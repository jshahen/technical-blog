---
title: 'Design Patterns'
---

References: [[1]], [[2]], [[3]]

## Withdrawal from Contracts


## Restricting Access


## State Machine

## Speed Bumps - Delay Contract Actions


## Contract Self Destruction


## Factory Contract


## Name Registry


## Mapping Iterator


## Sending ether from contract: Withdrawal pattern


[1]: https://solidity.readthedocs.io/en/v0.5.13/common-patterns.html
[2]: https://medium.com/heartbankstudio/smart-contract-design-patterns-8b7ca8b80dfb
[3]: https://medium.com/@i6mi6/solidty-smart-contracts-design-patterns-ecfa3b1e9784
[4]: https://www.sitepoint.com/smart-contract-safety-best-practices-design-patterns/