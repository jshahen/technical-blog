---
title: 'Research Topics'
---

## List of Random Ideas

1. Identity problem -- How much connecting information should we have between a real person and an account address
1. Privacy problem -- How can I get money without reveiling my account balance and log of all transactions
    * How can we prevent blackmail
        * Scenario: I send money to someone, and then review their transaction history to find any information that I can use to gain leverage on them
        * Scenario: if using etherium for voting, how to prevent a political attacker if a vote is attached to a persons
1. Bound problem -- How can we verify without stealing, 
    * The purchaser needs to verify that the work:
        * done correctly
        * works as efficiently as expected
        * does not contain any extra information (polictical messages, trojans, etc...)
    * The seller need to verify:
        * That the seller cannot steal your property without paying for it
        * Cannot change requirements
1. How well does Coin-Mixing work?
    * **Coin-Mixing:** a system where we can deposit ether into an account and then later on that system will send that money to an address that is not stored in the block-chain, thus does not link the 2 accounts together
    * How secure is a coin-mixer?
        * How many users are required?
        * How many transactions/block are required?
    * List of coin mixers:
        * Salad -- https://github.com/enigmampc/salad
        * ETHEREUM (ETH) MIXER -- https://bitcoinmix.org/eth
        * MixETH -- https://eprint.iacr.org/2019/341.pdf
        * Tornado.Cash
            * another Ethereum mixer, requires users to securely store a secret note which they will be required to submit when they wish to withdraw their funds.
    * Blog post: https://hackmd.io/@HWeNw8hNRimMm2m2GH56Cw/rJj9hEJTN?type=view
1. Non-Fungible Tokens (NFT)
    * blog post: https://medium.com/bridgeprotocol/non-fungible-tokens-nft-for-identity-and-how-it-all-works-b4548432144
    * ERC-721 technical document: http://erc721.org/
1. NuCypher
    * proxy reencryption -- change encryption key without decrypting
    * https://www.nucypher.com/proxy-re-encryption/
1. Skale ML
    * You can run a pretrain ml model on the blockchain
    * https://skale-me.github.io/skale/machine-learning/
1. Skale decentralized Storage
    * https://skale.network/blog/skale_decentralized_storage
1. InterPlanetary File System (IPFS)
    * white paper: https://ipfs.io/ipfs/QmR7GSQM93Cx5eAg6a6yRzNde1FQv7uL6X1o4k7zrJa3LX/ipfs.draft3.pdf
    * main paper: https://ipfs.io/
1. Crowd Funding and Initial Coin Offering (ICO)
    * How can we start a new project and collect investors?
    * What problems exists within this situation?
1. How large can the Etherium blockcahin get?
    * How large can it get before searching becomes very slow?
    * How large before storing it takes too many GBs?
    * How large before it becomes unfeasible to transfer the blockchain for a new node?

## Challenge Problems Ethereum has Posted
Link: https://challenges.ethereum.org/

## Forum with many problems
Link: https://ethresear.ch/

## Talk: I-B-C, Easy as N-F-T (Baby, you and me)
* Inter Blockchain Communication (IBC)
* An internet of blockchains; interconnected blockchains
* I can't think of any **interesting problems**

## Attacking Software License Contract
* Link: https://github.com/cryppadotta/dotta-license
* Can we not buy 1 license and then just share the private key to everyone?
* What is stopping a license from connecting at the same time? 
    * Central server? Heartbeat? Online connection required?
    * Does this provide any extra security, or is it just a distribution platform?

## Talk: Managing secrets in your DApp
* From NuCypher
* Reencyrption key uses Alice's private key and Bob's public key (On Alice's Client)
* Key-encapsulation: encrypt a symmtric key (the symmetric key encrypts the DATA), reencrypt the symmtric key to send to only BOB
* This solves the problem where we do not trust the proxy
* They revoke access between proxy and the reencryption key
    * Need to change the symmtric key for encyrption for the data
* Umbral scheme: shamir secret sharing (split key into 5 pieces and only require 3 different pieces to decrypt)
* Highlights:
    1. Dump your data on some service (amazon, google) and then go offline
    1. Setup a data provider
    1. Revoke Bob's access to data

## AZTEC Protocol
* Whitepaper: https://github.com/AztecProtocol/AZTEC/blob/develop/AZTEC.pdf
* An Introduction to AZTEC -- https://medium.com/aztec-protocol/an-introduction-to-aztec-47c70e875dc7


## Talk: From Single-Collateral Dai to Multi-Collatoral Dai
* no interesting topics


## Talk: An Introduction to Privacy on Ethereum
* AZTEC Protocol
* why is privacy important on the blockchain?
    * Does the cashier know how much money you make?
        * Can charge you more
    * Does your boss know how much money you are putting away?
        * They can use that to reduce your pay
* Long time for zero knowledge proofs to generate
* tiny.cc/aztec-sdk-docs
* able to prove certain knowledge without giving all your transaction history:
    * which university you went too
    * what age you are
    * being able to pay a certain enough money
* UTXO based?
    * https://blockonomi.com/utxo-vs-account-based-transaction-models/
    * https://medium.com/@sunflora98/utxo-vs-account-balance-model-5e6470f4e0cf
    * https://medium.com/nervosnetwork/my-comparison-between-the-utxo-and-account-model-821eb46691b2
* Includes a viewing-key which is diffy-hellman
* Only the amounts are kept secret? Which means that the from and to addresses can be seen on the block chain?
    * He says that full privacy is in the future

