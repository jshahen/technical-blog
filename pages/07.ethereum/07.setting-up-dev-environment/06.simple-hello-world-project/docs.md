---
title: 'Simple Hello World Project'
---

In this section I will outline how to get a simple hello world style etherium project setup


## Setting up with Truffle
Make sure you have nodejs installed, and `npm install -g truffle`.
Then make sure that nodejs global npm directory is on the system's path.

Create a new folder for your project and run the command: `truffle init .` (while inside the folder)

```bash
> mkdir test
> cd test
> truffle init .
```

### Files that are Created


## Writing a Hello World Smart Contract
To create the contract, use this command:
```bash
> truffle create contract HelloWorld
```

Open the file `contracts/HelloWorld.sol` and replace it with the following:

This smart contract is taken from https://techbrij.com/hello-world-smart-contract-solidity-ethereum-dapp-part-1
```solidity
pragma solidity ^0.5.0;
 
contract HelloWorld {
     
string defaultName;
mapping (address => string) public accounts;
 
constructor() public{
    defaultName = 'World';
}
 
function getMessage() public view returns(string memory){
    string memory name = bytes(accounts[msg.sender]).length > 0 ? accounts[msg.sender] : defaultName;
    return concat("Hello " , name);
}
 
 
function setName(string memory name) public returns(bool success){
    require(bytes(name).length > 0);
    accounts[msg.sender] = name;
    return true;
}
 
 function concat(string memory _base, string memory _value) internal pure returns (string memory) {
        bytes memory _baseBytes = bytes(_base);
        bytes memory _valueBytes = bytes(_value);
 
        string memory _tmpValue = new string(_baseBytes.length + _valueBytes.length);
        bytes memory _newValue = bytes(_tmpValue);
 
        uint i;
        uint j;
 
        for(i=0; i<_baseBytes.length; i++) {
            _newValue[j++] = _baseBytes[i];
        }
 
        for(i=0; i<_valueBytes.length; i++) {
            _newValue[j++] = _valueBytes[i];
        }
 
        return string(_newValue);
    }
}
```

## Compiling HelloWorld Contract


```bash
> truffle compile
```

## Creating the Migration for HelloWorld

```bash
> truffle create migration HelloWorld
```

## Setting up Development Blockchain

### Truffle's Commandline
```bash
> truffle develop
```

This will run the client on http://127.0.0.1:9545. It will display the first 10 accounts and the mnemonic used to create those accounts.

### Truffle's GUI
Truffle has a gui called [Ganache](https://www.trufflesuite.com/ganache), install on your computer and start it up. Once it is started, you will have a development Etherium blockchain setup and running.

You can change settings (ip/port and users) with the settings button.

## Writing a Test
Tests can be written in javascript or solidity, most programmers use javascript.

The test framework that we will use is [Moch JS](https://mochajs.org/)


## Tutorial Website for Developers
https://studio.ethereum.org/

