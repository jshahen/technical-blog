---
title: 'Setting up Development Environment'
---

## Programming Language (Solidity)
Similar to JavaScript, but some fundamental differences. Solidity is a compiled language and is a typed lanaguge.

Document can be found here: https://solidity.readthedocs.io/en/v0.5.12/

Multiple contracts can exists within a single solidity file.

```solidity
pragma solidity ^0.5.0;

contract ContractA {}

contract ContractB {}
```

## Development Environment for Smart Contracts (Remix)
Nothing required to be installed, just go to https://remix.ethereum.org/


## Testing Smart Contracts (Truffle)
You will need to install nodejs (https://nodejs.org/en/ -- install the LTS if you don't know which to pick)