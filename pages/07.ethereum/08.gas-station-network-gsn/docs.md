---
title: 'Gas Station Network (GSN)'
---

The Gas Station Network (GSN for short) is a decentralized solution for solving user onboarding to Ethereum applications. It allows dapps to pay for their users' transactions in a secure way, so users don’t need to hold ETH to pay for their gas or even set up an account.

The GSN was originally designed by Tabookey, and then further developed by OpenZeppelin. Today, it is backed by the Gas Station Network Alliance, a group of companies that adhere to and push the GSN standard.

The OpenZeppelin Platform provides full GSN support across the entire stack, so you can easily start developing a GSN-capable application. But before going through the components below, make sure you go check out our GSN introductory guide.

## Sending gas-less transactions
All Ethereum transactions use gas, and the sender of each transaction must have enough Ether to pay for the gas spent. Even though these gas costs are low for basic transactions (a couple of cents), getting Ether is no easy task: dApp users often need to go through Know Your Customer and Anti Money-Laundering processes (KYC & AML), which not only takes time but often involves sending a selfie holding their passport over the Internet (!). On top of that, they also need to provide financial information to be able to purchase Ether through an exchange. Only the most hardcore users will put up with this hassle, and dApp adoption greatly suffers when Ether is required. We can do better.

Enter meta-transactions. This is a fancy name for a simple idea: a third-party can send another user’s transactions and pay themselves for the gas cost. That’s it! There’s some tricky technical details, but those can be safely ignored when interacting with the GSN. This way, instead of your users calling into your contract (called the recipient) directly, someone else (we’ll call them a relayer) will send their transaction and pay for the cost.

But why would they do such a thing?

## Incentives
Relayers are not running a charity: they’re running a business. The reason why they’ll gladly pay for your users' gas costs is because they will in turn charge your contract, the recipient. That way relayers get their money back, plus a bit extra as a fee for their services.

This may sound strange at first, but paying for user onboarding is a very common business practice. Lots of money is spent on advertising, free trials, new user discounts, etc., all with the goal of user acquisition. Compared to those, the cost of a couple of Ethereum transactions is actually very small.

Additionally, you can leverage the GSN in scenarios where your users pay you off-chain in advance (e.g. via credit card), with each GSN-call deducting from their balance on your system. The possibilities are endless!

## Should I trust these relayers?
You don’t need to! The GSN is set up in such a way where it’s in the relayers' best interest to serve your requests, and there are measures in place to penalize them if they misbehave. All of this happens automatically, so you can safely start using their services worry-free.

## One contract to coordinate them all
There are many meta-transaction implementations out there, but the GSN has a unique detail that makes it special. Inside its core, a smart contract is responsible for keeping track of relayers, handling relayed transactions, charging their recipients, and generally ensuring all parties stay honest. This contract is called RelayHub, and there is a single instance of it in the whole network (you don’t need to deploy your own!). Think of it as a piece of public infrastructure, for all Ethereum users to benefit from.

One of RelayHub’s jobs is to act as a, well, hub for all relayers: they will advertise their services on this contract, and your users will query it to find the relayer that best suits their purposes. This is out of scope for this guide however, and is not something you need to worry about when writing a recipient contract. If you want to learn more about sending transactions via relayers, head to our GSNProvider guide.

The other key task RelayHub carries out is the actual relaying of transactions, the sole purpose behind this whole system. Instead of calling a function in your contract directly, your users will request a relayer to do it for them, who will then execute RelayHub’s relayCall function. RelayHub will verify that the transaction is legitimate (protecting both users and recipients from dishonest relayers), and then call into your contract as originally requested by your user. This requires your recipient trusting RelayHub to do the right thing, but since it is a smart contract, this is as simple as reading its source code!

References:
* [[1]]

[1]: https://docs.openzeppelin.com/openzeppelin/gsn/getting-started.html