---
title: 'Hackathon Project Ideas'
---

## ATRBAC on the Blockchain
* Create an implementation of ATRBAC smart contract on the blockchain

### Static ATRBAC Policy
* Do not allow the owner to update anything after publishing
* Allow an initialation of roles (roles that are given to trusted addresses)


### Dynamic ATRBAC Policy
* Allow the owner to update the ruleset and the TRBAc state (bypasses rules)

## Anonymous Peer-to-peer Chat