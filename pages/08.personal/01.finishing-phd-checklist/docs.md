---
title: 'Finishing PhD Checklist'
---

Below is a mindmap and gnatt chart of the projects and milestones I require to finish before September 2020.

## Gnatt Chart
[mermaid]
gantt
	title Finishing PhD
	dateFormat  YYYY-MM-DD
	section ECE657A
        A1  : done, 657a_a1, 2020-01-20, 2020-02-02
        A2  : active, 657a_a2, 2020-02-10, 2020-03-09
        REVIEW  : 657a_review, 2020-03-24, 2020-04-03
        RESEARCH    : 657a_reser, 2020-03-24, 2020-04-03
        PRESENTATION   : 657a_pres, 2020-03-24, 2020-04-03
        FINAL   : 657a_final, 2020-04-08,2020-04-25
	section ECE602
        A1  : 602_a1, 2020-02-28, 2020-03-13
        A2  : 602_a2, 2020-03-11, 2020-03-20
        A3  : 602_a3, after 602_a2, 2w
        A4  : 602_a4, after 602_a3, 2w
        A5  : 602_a5, after 602_a4, 2w
        Project   : 602_proj, 2020-03-11, 2020-04-03
        FINAL   : 602_final, 2020-04-08,2020-04-25
    section PhD Seminar
        Schedule Date: active,PHDS_SD,2020-02-10,2020-03-10
        Create Presentation: active,PHDS_CP,2020-02-10,3w
        Give Seminar: PHDS_GS,2020-05-19,2020-05-20
    section PhD Defence
        Talk with Mahesh about External: active, PHDD_TALK, 2020-02-10, 1w
        Schedule Date: PHDD_SD, 2020-02-25, 2020-03-18
        Write Thesis: active,PHDD_WRITE, 2020-01-01, 2020-06-01
        Submit Thesis: active,PHDD_SUBMIT, 2020-06-03, 2020-06-04
        Prepare for Defence: PHDD_PD,2020-06-01, 2020-07-08
        Defend Thesis: PHDD_DEFEND,2020-07-09, 2020-07-10
    section Research
        Logic Locking Data: active, LOGICLOCK, 2020-02-12, 2020-03-05
    section Work
        Take Professional Photo for Resume: W_PHOTO, 2020-03-15,1w
        Update Websites: W_UPDATE, after W_PHOTO, 1w
        Apply For Jobs: W_APPLY, 2020-04-25, 2020-08-31
        Prepare for Interviews: W_PREP, 2020-04-02, 1w
        Interview for Jobs: W_INTERVIEW, after W_PREP, 2020-08-31
[/mermaid]

## Mindmaps
[mermaid]
graph TD
    DR[Degree Requirements]
        DR --> PHDS[PhD Seminar]
            PHDS --> PHDS_SD[Schedule Date]
            PHDS --> PHDS_CP[Create Presentation]
            PHDS --> PHDS_GS[Give Seminar]
        DR --> PHDD[PhD Defence]
            PHDD --> PHDD_SD[Schedule Date]
            PHDD --> PHDD_PD[Prepare for Defence]
            PHDD --> PHDD_TALK[Talk with Mahesh about External]
            PHDD --> PHDD_DEFEND[Defend Thesis]
            PHDD --> PHDD_WRITE[Write Thesis]
[/mermaid]
[mermaid]
graph TD
    C[Courses]
        C --> 657A[ECE 657A]
            657A --> 657A_A1[A1]
            657A --> 657A_A2[A2]
            657A --> 657A_A3[A3]
            657A --> 657A_REVIEW[Paper Review]
            657A --> 657A_RESEARCH[Research Paper]
            657A --> 657A_PRESENTATIONM[Presentation]
            657A --> 657A_FINAL[Final Exam]
        C --> 602[ECE 602]
            602 --> 602_A1[A1]
            602 --> 602_A2[A2]
            602 --> 602_A3[A3]
            602 --> 602_A4[A4]
            602 --> 602_A5[A5]
            602 --> 602_PROJECT[Project]
            602 --> 602_FINAL[Final Exam]
    style 657A_A1 fill:green,color:white,stroke-width:2px,stroke:black
[/mermaid]
[mermaid]
graph TD
    W[Work]
        W --> W_APPLY[Apply For Jobs]
        W --> W_PREP[Prepare for Interviews]
        W --> W_PHOTO[Take Professional Photo for Resume]
        W --> W_INTERVIEW[Interview for Jobs]
        W --> W_UPDATE[Update Websites]
[/mermaid]